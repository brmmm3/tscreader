use std::str;
use std::path::PathBuf;
use std::io::Error;

use pyo3::prelude::*;
use pyo3::types::{PyType, PyBytes, PyString};
use pyo3::exceptions::PyValueError;

use tscreader::{TscReader, TscReturnValue, TscReturnValueUtf8};

#[pyclass]
#[derive(Debug)]
pub struct Instance {
    pub path: PathBuf,
    pub reader: TscReader,
    pub errors: Vec<Error>,
}

#[pymethods]
impl Instance {
    #[new]
    fn __new__(
        path: &str,
        trim: Option<bool>,
        line_end: Option<u8>
    ) -> PyResult<Self> {
        Ok(Instance {
            path: PathBuf::from(path),
            reader: TscReader::new(path, trim, line_end),
            errors: Vec::new()
        })
    }

    fn __enter__(slf: PyRefMut<Self>) -> PyResult<PyRefMut<Self>> {
        Ok(slf)
    }

    fn __exit__(
        mut slf: PyRefMut<Self>,
        ty: Option<&PyType>,
        _value: Option<&PyAny>,
        _traceback: Option<&PyAny>,
    ) -> PyResult<bool> {
        if let Err(e) = slf.reader.stop() {
            return Err(PyValueError::new_err(format!("{}", e)));
        }
        if ty == Some(slf.py().get_type::<PyValueError>()) {
            Ok(true)
        } else {
            Ok(false)
        }
    }

    fn stop(mut slf: PyRefMut<Self>) -> PyResult<bool> {
        if let Err(e) = slf.reader.stop() {
            return Err(PyValueError::new_err(format!("{}", e)));
        }
        Ok(true)
    }

    fn next(mut slf: PyRefMut<Self>) -> PyResult<PyObject> {
        match slf.reader.next() {
            Some(s) => match s {
                Ok(v) => match v {
                    TscReturnValue::Text(w) => return Ok(PyBytes::new(slf.py(), &w).into()),
                    TscReturnValue::String(w) => return Ok(PyBytes::new(slf.py(), &w).into()),
                    TscReturnValue::Comment(w) => return Ok(PyBytes::new(slf.py(), &w).into()),
                    TscReturnValue::LineComment(w) => return Ok(PyBytes::new(slf.py(), &w).into()),
                },
                Err(e) => return Err(PyValueError::new_err(format!("{}", e)))
            },
            None => return Ok(slf.py().None())
        }
    }

    fn next_utf8(mut slf: PyRefMut<Self>) -> PyResult<PyObject> {
        match slf.reader.next_utf8() {
            Some(s) => match s {
                Ok(v) => match v {
                    TscReturnValueUtf8::Text(w) => return Ok(PyString::new(slf.py(), &w).into()),
                    TscReturnValueUtf8::String(w) => return Ok(PyString::new(slf.py(), &w).into()),
                    TscReturnValueUtf8::Comment(w) => return Ok(PyString::new(slf.py(), &w).into()),
                    TscReturnValueUtf8::LineComment(w) => return Ok(PyString::new(slf.py(), &w).into()),
                },
                Err(e) => return Err(PyValueError::new_err(format!("{}", e)))
            },
            None => return Ok(slf.py().None())
        }
    }

    fn next_utf8_lossy(mut slf: PyRefMut<Self>) -> PyResult<PyObject> {
        match slf.reader.next_utf8_lossy() {
            Some(s) => match s {
                Ok(v) => match v {
                    TscReturnValueUtf8::Text(w) => return Ok(PyString::new(slf.py(), &w).into()),
                    TscReturnValueUtf8::String(w) => return Ok(PyString::new(slf.py(), &w).into()),
                    TscReturnValueUtf8::Comment(w) => return Ok(PyString::new(slf.py(), &w).into()),
                    TscReturnValueUtf8::LineComment(w) => return Ok(PyString::new(slf.py(), &w).into()),
                },
                Err(e) => return Err(PyValueError::new_err(format!("{}", e)))
            },
            None => return Ok(slf.py().None())
        }
    }

    #[getter]
    fn path(slf: PyRef<Self>) -> PyResult<String> {
        Ok(slf.path.clone().into_os_string().into_string().unwrap())
    }

    #[getter]
    fn line_nr(slf: PyRef<Self>) -> PyResult<usize> {
        Ok(slf.reader.line_nr())
    }

    #[getter]
    fn errors(slf: PyRef<Self>) -> PyResult<Vec<String>> {
        let mut errors: Vec<String> = Vec::new();
        for error in slf.errors.iter() {
            errors.push(format!("{}", error));
        }
        Ok(errors)
    }
}

#[pymodule]
fn pytscreader(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add("__version__", env!("CARGO_PKG_VERSION"))?;
    m.add_class::<Instance>()?;
    Ok(())
}
