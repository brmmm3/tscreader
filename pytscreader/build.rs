fn main() {
    for each in vec!["3.8.6"] {
        println!("cargo:rustc-env=PYENV_VERSION={}", each);
    }
}
