
import pytscreader as reader


with reader.Instance("./res/empty.txt") as R:
    while s := R.next():
        print(f"#{s}#")
