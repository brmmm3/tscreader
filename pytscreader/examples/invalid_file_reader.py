
import pytscreader as reader


with reader.Instance("./res/invalid.txt") as R:
    while s := R.next():
        print(f"#{s}#")
