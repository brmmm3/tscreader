
import pytscreader as reader


print("Read whole file as bytes...")
with reader.Instance("./res/example.txt") as R:
    while s := R.next():
        print(f"#{s}#")

print("Read whole file as UTF8...")
with reader.Instance("./res/example.txt") as R:
    while s := R.next_utf8():
        print(f"#{s}#")

print("Read whole file as UTF8 with errors allowed...")
with reader.Instance("./res/example.txt") as R:
    while s := R.next_utf8_lossy():
        print(f"#{s}#")

print("\nRead just 10 items...")
with reader.Instance("./res/example.txt") as R:
    for _ in range(10):
        s = R.next()
        print(f"#{s}#")
