//
// Copyright 2016 The IHEX Developers. See the COPYRIGHT
// file at the top-level directory of this distribution.
//
// Licensed under the MIT license <LICENSE-MIT or http://opensource.org/licenses/MIT>.
// All files in the project carrying such notice may not be copied, modified, or
// distributed except according to those terms.
//

use tscreader::*;

#[test]
fn test_basic_loop() {
    let mut reader = TscReader::new("../res/example.txt", None, None);
    let mut texts = Vec::new();
    let mut strings = Vec::new();
    let mut comments = Vec::new();
    let mut line_comments = Vec::new();

    while let Some(Ok(item)) = reader.next() {
        match item {
            TscReturnValue::Text(v) => texts.push(String::from_utf8(v).unwrap()),
            TscReturnValue::String(v) => strings.push(String::from_utf8(v).unwrap()),
            TscReturnValue::Comment(v) => comments.push(String::from_utf8(v).unwrap()),
            TscReturnValue::LineComment(v) => line_comments.push(String::from_utf8(v).unwrap()),
        }
    }

    assert_eq!(texts.len(), 29);
    assert_eq!(strings.len(), 6);
    assert_eq!(comments.len(), 4);
    assert_eq!(line_comments.len(), 5);
}

#[test]
fn test_basic_iterator() {
    let reader = TscReader::new("../res/example.txt", None, None);
    let mut texts = Vec::new();
    let mut strings = Vec::new();
    let mut comments = Vec::new();
    let mut line_comments = Vec::new();

    for item in reader.into_iter() {
        match item {
            Ok(i) => match i {
                TscReturnValue::Text(v) => texts.push(String::from_utf8(v).unwrap()),
                TscReturnValue::String(v) => strings.push(String::from_utf8(v).unwrap()),
                TscReturnValue::Comment(v) => comments.push(String::from_utf8(v).unwrap()),
                TscReturnValue::LineComment(v) => line_comments.push(String::from_utf8(v).unwrap()),
            },
            Err(e) => {
                eprint!("{}", e);
                break;
            }
        }
    }

    assert_eq!(texts.len(), 29);
    assert_eq!(strings.len(), 6);
    assert_eq!(comments.len(), 4);
    assert_eq!(line_comments.len(), 5);
}

#[test]
fn test_empty_line() {
    let reader = TscReader::new("./res/empty.txt", None, None);
    let mut texts = Vec::new();
    let mut strings = Vec::new();
    let mut comments = Vec::new();
    let mut line_comments = Vec::new();

    for item in reader.into_iter() {
        match item {
            Ok(i) => match i {
                TscReturnValue::Text(v) => texts.push(String::from_utf8(v).unwrap()),
                TscReturnValue::String(v) => strings.push(String::from_utf8(v).unwrap()),
                TscReturnValue::Comment(v) => comments.push(String::from_utf8(v).unwrap()),
                TscReturnValue::LineComment(v) => line_comments.push(String::from_utf8(v).unwrap()),
            },
            Err(e) => {
                eprint!("{}", e);
                break;
            }
        }
    }

    assert_eq!(texts.len(), 0);
    assert_eq!(strings.len(), 0);
    assert_eq!(comments.len(), 0);
    assert_eq!(line_comments.len(), 0);
}

#[test]
fn test_invalid_filename() {
    let mut reader = TscReader::new("./res/invalid.txt", None, None);
    let mut texts = Vec::new();
    let mut strings = Vec::new();
    let mut comments = Vec::new();
    let mut line_comments = Vec::new();

    while let Some(item) = reader.next() {
        match item {
            Ok(i) => match i {
                TscReturnValue::Text(v) => texts.push(String::from_utf8(v).unwrap()),
                TscReturnValue::String(v) => strings.push(String::from_utf8(v).unwrap()),
                TscReturnValue::Comment(v) => comments.push(String::from_utf8(v).unwrap()),
                TscReturnValue::LineComment(v) => line_comments.push(String::from_utf8(v).unwrap()),
            },
            Err(e) => {
                eprint!("{}", e);
                break;
            }
        }
    }

    assert_eq!(texts.len(), 0);
    assert_eq!(strings.len(), 0);
    assert_eq!(comments.len(), 0);
    assert_eq!(line_comments.len(), 0);
}
