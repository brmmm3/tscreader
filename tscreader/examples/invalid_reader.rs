use tscreader::*;

fn main() {
    let mut reader = TscReader::new("./res/invalid.txt", None, None);
    let mut texts = Vec::new();
    let mut strings = Vec::new();
    let mut comments = Vec::new();
    let mut line_comments = Vec::new();

    while let Some(word) = reader.next() {
        match word {
            Ok(w) => {
                println!("{:?}", w);
                match w {
                    TscReturnValue::Text(v) => texts.push(String::from_utf8(v).unwrap()),
                    TscReturnValue::String(v) => strings.push(String::from_utf8(v).unwrap()),
                    TscReturnValue::Comment(v) => comments.push(String::from_utf8(v).unwrap()),
                    TscReturnValue::LineComment(v) => line_comments.push(String::from_utf8(v).unwrap()),
                };
            },
            Err(e) => {
                eprintln!("ERROR: {}", e);
                break;
            }
        }
    }
    println!("texts={} {:?}", texts.len(), texts);
    println!("strings={} {:?}", strings.len(), strings);
    println!("comments={} {:?}", comments.len(), comments);
    println!("line_comments={} {:?}", line_comments.len(), line_comments);
}
