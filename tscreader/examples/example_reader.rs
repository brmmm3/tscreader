use tscreader::*;

fn main() {
    let mut reader = TscReader::new("./res/example.txt", None, None);
    let mut texts = Vec::new();
    let mut strings = Vec::new();
    let mut comments = Vec::new();
    let mut line_comments = Vec::new();

    while let Some(word) = reader.next_utf8() {
        match word {
            Ok(w) => {
                println!("{}: {:?}", reader.line_nr(), w);
                match w {
                    TscReturnValueUtf8::Text(v) => texts.push(v),
                    TscReturnValueUtf8::String(v) => strings.push(v),
                    TscReturnValueUtf8::Comment(v) => comments.push(v),
                    TscReturnValueUtf8::LineComment(v) => line_comments.push(v),
                };
            },
            Err(e) => {
                eprintln!("ERROR: {}", e);
                break;
            }
        }
    }
    println!("texts={} {:?}", texts.len(), texts);
    println!("strings={} {:?}", strings.len(), strings);
    println!("comments={} {:?}", comments.len(), comments);
    println!("line_comments={} {:?}", line_comments.len(), line_comments);
}
