//! `tscreader` is a simple library crate offering an iterator which splits a file into
//! - text sequences, separated by characters with ASCII codes <=32 and >=127.
//! - strings with double quotes as delimiters.
//! - line comments with '//' as start sequence.
//! - comment blocks with '/*' as start sequence and '*/' as end sequence.
//!
//! Results can be obatined as bytes sequence (`next` method) or UTF8 string (`next_utf8` or `next_utf8_lossy` method).
//! Property `line_nr` provides the current line in the text file.
#![doc(html_root_url = "https://docs.rs/tscreader/0.1.0")]
use std::io::{Error, ErrorKind};
use std::io::prelude::*;
use std::thread::{self, JoinHandle};
use std::fs::File;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

use flume::{Sender, Receiver, bounded};


#[derive(Debug)]
pub enum TscReturnValue {
    Text(Vec<u8>),
    String(Vec<u8>),
    Comment(Vec<u8>),
    LineComment(Vec<u8>),
}

#[derive(Debug)]
pub enum TscReturnValueUtf8 {
    Text(String),
    String(String),
    Comment(String),
    LineComment(String),
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
enum ReaderStates {
    Default,
    Slash,
    LineComment,
    Comment,
    CommentAsterisk,
    String,
}

/// Provides iteration over bytes or utf8 string of words, strings and (line) comments.
///
/// ```rust
/// use tscreader::*;
///
/// // construct our iterator from our file input
/// let mut reader = TscReader::new("../res/numbers.txt", None, None);
///
/// // walk our item using `while` syntax
/// while let Some(item) = reader.next() {
///     // do something with the item, which is Result<&[u8], _>
/// }
/// ```
///
/// For those who prefer the `Iterator` API, this structure implements
/// the `IntoIterator` trait to provide it. This comes at the cost of
/// an allocation of a `Vec` for each line in the `Iterator`. This is
/// negligible in many cases, so often it comes down to which syntax
/// is preferred:
///
/// ```rust
/// use tscreader::*;
///
/// // construct our iterator from our file input
/// let reader = TscReader::new("./res/example.txt", None, None);
///
/// // walk our items using `for` syntax
/// for item in reader.into_iter() {
///     // do something with the item, which is Result<TscReturnValue, Error>
/// }
/// ```
#[derive(Debug)]
pub struct TscReader
{
    thread_handle: Option<JoinHandle<Result<(), Error>>>,
    items: Receiver<Option<(usize, Result<TscReturnValue, Error>)>>,
    stop: Arc<AtomicBool>,
    line_num: usize,
    reader_died: bool,
}

impl TscReader
{
    /// Constructs a new `TscReader`.
    pub fn new(filename: &str, trim: Option<bool>, line_end: Option<u8>) -> Self {
        TscReader::with_capacity(filename, 65536, trim, line_end)
    }

    /// Constructs a new `TscReader`.
    pub fn with_capacity(filename: &str, size: usize, trim: Option<bool>, line_end: Option<u8>) -> Self {
        let (tx, rx) = bounded(64);
        let stop = Arc::new(AtomicBool::new(false));
        Self {
            thread_handle: Some(TscReader::reader_thread(
                filename,
                size,
                trim.unwrap_or(false),
                line_end.unwrap_or(b'\n'),
                tx,
                stop.clone())),
                reader_died: false,
            items: rx,
            stop,
            line_num: 0,
        }
    }

    fn reader_thread(filename: &str,
                     size: usize,
                     trim: bool,
                     line_end: u8,
                     tx: Sender<Option<(usize, Result<TscReturnValue, Error>)>>,
                     stop: Arc<AtomicBool>) -> JoinHandle<Result<(), Error>> {
        let filename = filename.to_owned();
        thread::spawn(move || {
            let mut file = match File::open(filename) {
                Ok(fh) => fh,
                Err(e) => {
                    if let Err(_) = tx.send(Some((0, Err(e)))) {
                        return Err(Error::new(ErrorKind::BrokenPipe, "Failed to send error message for failed open file!"));
                    }
                    return Ok(());
                }
            };
            let mut buffer = vec![0u8; size];
            let mut offset = 0;
            let mut start = None;
            let mut state = ReaderStates::Default;
            let mut escape = false;
            let mut line_num = 1;
            loop {
                if stop.load(Ordering::SeqCst) {
                    break;
                }
                let nread = match file.read(&mut buffer[offset..]) {
                    Ok(num) => num,
                    Err(e) => {
                        if let Err(_) = tx.send(Some((line_num, Err(e)))) {
                            return Err(Error::new(ErrorKind::BrokenPipe, "Failed to send error message for file read!"));
                        }
                        break;
                    }
                };
                if nread == 0 {
                    if offset > 0 {
                        let data = (&buffer[..offset + 1]).iter().cloned().collect();
                        if let Err(e) = match state {
                            ReaderStates::Default => tx.send(Some((line_num, Ok(TscReturnValue::Text(data))))),
                            ReaderStates::String => tx.send(Some((line_num, Ok(TscReturnValue::String(data))))),
                            ReaderStates::Comment => tx.send(Some((line_num, Ok(TscReturnValue::Comment(data))))),
                            ReaderStates::LineComment => tx.send(Some((line_num, Ok(TscReturnValue::LineComment(data))))),
                            _ => Ok(())
                        } {
                            return Err(Error::new(ErrorKind::BrokenPipe, format!("Failed to send result: {}", e)));
                        }
                    }
                    break;
                }

                let buf = &mut buffer[..offset + nread];

                for i in (0..buf.len()).skip(offset) {
                    if stop.load(Ordering::SeqCst) {
                        break;
                    }
                    let c = buf[i];
                    match state {
                        ReaderStates::Default => {
                            if c <= b' ' || c >= b'\x7f' {
                                if let Some(s) = start.take() {
                                    if let Err(e) = tx.send(Some((line_num, Ok(TscReturnValue::Text((&buf[s..i]).iter().cloned().collect()))))) {
                                        return Err(Error::new(ErrorKind::BrokenPipe, format!("Failed to send text sequence: {}", e)));
                                    }
                                }
                            } else {
                                if c == b'/' {
                                    state = ReaderStates::Slash;
                                } else if c == b'"' {
                                    if let Some(s) = start.take() {
                                        if let Err(e) = tx.send(Some((line_num, Ok(TscReturnValue::Text((&buf[s..i]).iter().cloned().collect()))))) {
                                            return Err(Error::new(ErrorKind::BrokenPipe, format!("Failed to send text sequence: {}", e)));
                                        }
                                    }
                                    state = ReaderStates::String;
                                }
                                if start.is_none() {
                                    start = Some(i);
                                }
                            }
                        },
                        ReaderStates::String => {
                            if c == b'\\' {
                                escape = true;
                            } else if !escape && c == b'"' {
                                if let Some(mut s) = start.take() {
                                    let mut e = i;
                                    if trim {
                                        s += 1;
                                    } else {
                                        e += 1;
                                    }
                                    if let Err(e) = tx.send(Some((line_num, Ok(TscReturnValue::String((&buf[s..e]).iter().cloned().collect()))))) {
                                        return Err(Error::new(ErrorKind::BrokenPipe, format!("Failed to send string: {}", e)));
                                    }
                                }
                                state = ReaderStates::Default;
                            } else {
                                escape = false;
                            }
                        },
                        ReaderStates::Slash => {
                            if c != b'*' && c != b'/' {
                                if c == b'"' {
                                    if let Some(s) = start.take() {
                                        if let Err(e) = tx.send(Some((line_num, Ok(TscReturnValue::Text((&buf[s..i]).iter().cloned().collect()))))) {
                                            return Err(Error::new(ErrorKind::BrokenPipe, format!("Failed to send text sequence: {}", e)));
                                        }
                                    }
                                    start = Some(i);
                                    state = ReaderStates::String;
                                } else {
                                    if c <= b' ' || c >= b'\x7f' {
                                        if let Some(s) = start.take() {
                                            if let Err(e) = tx.send(Some((line_num, Ok(TscReturnValue::Text((&buf[s..i]).iter().cloned().collect()))))) {
                                                return Err(Error::new(ErrorKind::BrokenPipe, format!("Failed to send text sequence: {}", e)));
                                            }
                                        }
                                    }
                                    state = ReaderStates::Default;
                                }
                            } else {
                                if let Some(s) = start.take() {
                                    if s < i - 1 {
                                        if let Err(e) = tx.send(Some((line_num, Ok(TscReturnValue::Text((&buf[s..i - 1]).iter().cloned().collect()))))) {
                                            return Err(Error::new(ErrorKind::BrokenPipe, format!("Failed to send word: {}", e)));
                                        }
                                    }
                                    start = Some(i - 1);
                                }
                                if c == b'*' {
                                    state = ReaderStates::Comment;
                                } else {
                                    state = ReaderStates::LineComment;
                                }
                            }
                        },
                        ReaderStates::Comment => {
                            if c == b'*' {
                                state = ReaderStates::CommentAsterisk;
                            }
                        },
                        ReaderStates::CommentAsterisk => {
                            if c == b'/' {
                                if let Some(mut s) = start.take() {
                                    let mut e = i + 1;
                                    if trim {
                                        s += 2;
                                        e -= 2;
                                    }
                                    if let Err(e) = tx.send(Some((line_num, Ok(TscReturnValue::Comment((&buf[s..e]).iter().cloned().collect()))))) {
                                        return Err(Error::new(ErrorKind::BrokenPipe, format!("Failed to send comment: {}", e)));
                                    }
                                }
                                state = ReaderStates::Default;
                            }
                        },
                        ReaderStates::LineComment => {
                            if c < b' ' {
                                if let Some(mut s) = start.take() {
                                    if trim {
                                        s += 2;
                                    }
                                    if let Err(e) = tx.send(Some((line_num, Ok(TscReturnValue::LineComment((&buf[s..i]).iter().cloned().collect()))))) {
                                        return Err(Error::new(ErrorKind::BrokenPipe, format!("Failed to send line comment: {}", e)));
                                    }
                                }
                                state = ReaderStates::Default;
                            }
                        },
                    }
                    if c == line_end {
                        line_num += 1;
                    }
                }
                if let Some(ref mut s) = start {
                    offset = buf.len() - *s;
                    buf.copy_within(*s.., 0);
                    *s = 0;
                } else {
                    offset = 0;
                }
            }
            if let Err(_) = tx.send(None) {
                return Err(Error::new(ErrorKind::BrokenPipe, "Failed to send finish token!"));
            }
          Ok(())
        })
    }

    pub fn stop(&mut self) -> Result<(), Error> {
        self.stop.store(true, Ordering::SeqCst);
        let _ = self.items.try_recv();  // Read last entry
        let _ = self.items.try_recv();  // Read None
        match self.thread_handle.take() {
            Some(h) => match h.join() {
                Ok(result) => result,
                Err(e) => Err(Error::new(ErrorKind::Other, format!("Failed to join thread: {:?}", e)))
            },
            None => Err(Error::new(ErrorKind::Other, "No thread to stop!"))
        }
    }

    /// Retrieves a reference to the next word, string or (line) comment of bytes in the reader (if any).
    pub fn next(&mut self) -> Option<Result<TscReturnValue, Error>> {
        match self.items.recv() {
            Ok(Some((line_num, item))) => {
                self.line_num = line_num;
                Some(item)
            },
            Ok(None) => None,
            Err(e) => {
                if self.reader_died {
                    return None;
                }
                self.reader_died = true;
                Some(Err(Error::new(ErrorKind::BrokenPipe, format!("Reader thread died: {}", e))))
            }
        }
    }

    /// Retrieves a reference to the next word, string or (line) comment of bytes as utf8 in the reader (if any).
    pub fn next_utf8(&mut self) -> Option<Result<TscReturnValueUtf8, Error>> {
        match self.items.recv() {
            Ok(some) => match some {
                Some((line_num, item)) => {
                    self.line_num = line_num;
                    match item {
                        Ok(w) => {
                            match w {
                                TscReturnValue::Text(v) => Some(Ok(TscReturnValueUtf8::Text(String::from_utf8(v).unwrap()))),
                                TscReturnValue::String(v) => Some(Ok(TscReturnValueUtf8::String(String::from_utf8(v).unwrap()))),
                                TscReturnValue::Comment(v) => Some(Ok(TscReturnValueUtf8::Comment(String::from_utf8(v).unwrap()))),
                                TscReturnValue::LineComment(v) => Some(Ok(TscReturnValueUtf8::LineComment(String::from_utf8(v).unwrap()))),
                            }
                        },
                        Err(e) => Some(Err(e)),
                    }
                },
                None => None,
            },
            Err(e) => {
                if self.reader_died {
                    return None;
                }
                self.reader_died = true;
                Some(Err(Error::new(ErrorKind::BrokenPipe, format!("Reader thread died: {}", e))))
            }
        }
    }

    /// Retrieves a reference to the next word, string or (line) comment of bytes as utf8 in the reader (if any).
    pub fn next_utf8_lossy(&mut self) -> Option<Result<TscReturnValueUtf8, Error>> {
        match self.items.recv() {
            Ok(some) => match some {
                Some((line_num, item)) => {
                    self.line_num = line_num;
                    match item {
                        Ok(w) => {
                            match w {
                                TscReturnValue::Text(v) => Some(Ok(TscReturnValueUtf8::Text(String::from_utf8_lossy(&v).into()))),
                                TscReturnValue::String(v) => Some(Ok(TscReturnValueUtf8::String(String::from_utf8_lossy(&v).into()))),
                                TscReturnValue::Comment(v) => Some(Ok(TscReturnValueUtf8::Comment(String::from_utf8_lossy(&v).into()))),
                                TscReturnValue::LineComment(v) => Some(Ok(TscReturnValueUtf8::LineComment(String::from_utf8_lossy(&v).into()))),
                            }
                        },
                        Err(e) => Some(Err(e)),
                    }
                },
                None => None,
            },
            Err(e) => {
                if self.reader_died {
                    return None;
                }
                self.reader_died = true;
                Some(Err(Error::new(ErrorKind::BrokenPipe, format!("Reader thread died: {}", e))))
            }
        }
    }

    /// Returns the corresponding line in the file for the latest returned item.
    pub fn line_nr(&self) -> usize {
        self.line_num
    }
}

/// `IntoIterator` conversion for `TscReader` to provide `Iterator` APIs.
impl IntoIterator for TscReader
{
    type Item = Result<TscReturnValue, Error>;
    type IntoIter = TscReaderIter;

    /// Constructs a `tscreaderIter` to provide an `Iterator` API.
    #[inline]
    fn into_iter(self) -> TscReaderIter {
        TscReaderIter { inner: self }
    }
}

/// `Iterator` implementation of `TscReader` to provide `Iterator` APIs.
///
/// This structure enables developers the use of the `Iterator` API in
/// their code, at the cost of an allocation per returned item:
///
/// ```rust
/// use tscreader::*;
///
/// // construct our iterator from our file input
/// let reader = TscReader::new("../res/example.txt", None, None);
///
/// // walk our items using `for` syntax
/// for item in reader.into_iter() {
///     // do something with the item, which is Result<TscReturnValue, Error>
/// }
/// ```
pub struct TscReaderIter
{
    inner: TscReader,
}

impl Iterator for TscReaderIter
{
    type Item = Result<TscReturnValue, Error>;

    /// Retrieves the next item in the iterator (if any).
    #[inline]
    fn next(&mut self) -> Option<Result<TscReturnValue, Error>> {
        self.inner.next().map(|r| r.map(|s| s))
    }
}
